This is an **experimental** app to run in the background and optionally prompt the user to report crashes to app developers.

The goal is to allow users to help developers by opting in to sending crash reports, without developers having to depend on the Google ecosystem.
This will be done while maintaining users privacy and security.

## How does it work

This is a fork of the old but great alogcat app.
That app starts a `logcat` process and watches the output.

This fork does the same, except it:
 * Is designed to be installed in `/system/priv-app` to be able to access higher level logs.
 * Filters logs to only those related to crashes.
 * Parses the output of crashes to identify the process (i.e. package-ish name) which caused the crash.

Eventually, this will be compared to the list 

### Example logcat output

Here is some sample output from `adb logcat -v long -b crash`, the command used by this app to monitor crashes:

```
$ adb -e logcat -v long -b crash
[ 07-12 11:20:04.141  3100: 3100 E/AndroidRuntime ]
FATAL EXCEPTION: main
Process: org.fdroid.fdroid.crasher, PID: 3100
java.lang.NumberFormatException: For input string: "Crashing app"
	at java.lang.Integer.parseInt(Integer.java:521)
	at java.lang.Integer.parseInt(Integer.java:556)
	at org.fdroid.fdroid.crasher.MainActivity$1.onClick(MainActivity.java:19)
	at android.view.View.performClick(View.java:5609)
	at android.view.View$PerformClick.run(View.java:22267)
	at android.os.Handler.handleCallback(Handler.java:751)
	at android.os.Handler.dispatchMessage(Handler.java:95)
	at android.os.Looper.loop(Looper.java:154)
	at android.app.ActivityThread.main(ActivityThread.java:5969)
	at java.lang.reflect.Method.invoke(Native Method)
	at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:801)
	at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:691)


[ 07-12 11:24:51.160  3342: 3342 E/AndroidRuntime ]
FATAL EXCEPTION: main
Process: org.fdroid.fdroid.crasher, PID: 3342
java.lang.NumberFormatException: For input string: "Crashing app"
	at java.lang.Integer.parseInt(Integer.java:521)
	at java.lang.Integer.parseInt(Integer.java:556)
	at org.fdroid.fdroid.crasher.MainActivity$1.onClick(MainActivity.java:19)
	at android.view.View.performClick(View.java:5609)
	at android.view.View$PerformClick.run(View.java:22267)
	at android.os.Handler.handleCallback(Handler.java:751)
	at android.os.Handler.dispatchMessage(Handler.java:95)
	at android.os.Looper.loop(Looper.java:154)
	at android.app.ActivityThread.main(ActivityThread.java:5969)
	at java.lang.reflect.Method.invoke(Native Method)
	at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:801)
	at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:691)

```

## Installing to /system/priv-app on emulator

For this app to work, it needs to be installed in /system/priv-app on your device.
When testing in an emulator, the following steps need to be taken.
Once your debug build is in /system/priv-app once, then you need not do it each time you rebuild.
Instead, just `adb install` or use AndroidStudio to run the app afterwards.

 * Start emulator with the `-writable-system` argument
 * `adb install` or use Android Studio to install the app with regular permissions
 * `adb root` to allow us to remount partitions
 * `adb remount` to remount the /system partition (among others) as writeable
 * `adb shell mv /data/app/org.jtp.alogcat-1` /system/priv-app`, but check where the app directory actually is as it may differ.
 * Restart the emulator
