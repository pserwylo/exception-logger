package org.fdroid.fdroid.exceptionlogger;

import android.content.Context;
import android.util.Log;

@SuppressWarnings("WeakerAccess")
public class Reporter {
    public static void send(Context context, LoggedException exception) {
        Log.i("Crash Reporter", "Identified crash in package " + exception.getPackageName() + ": " + exception.body);
    }
}
