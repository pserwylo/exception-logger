package org.fdroid.fdroid.exceptionlogger;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Process;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class ExceptionLoggingService extends Service {

    private static final String TAG = "ExceptionLoggingService";

    private Logcat logcat = null;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Starting exception logging service.");
        final LogcatHandler handler = new LogcatHandler(this);

        new Thread() {
            public void run() {
                Process.setThreadPriority(Process.THREAD_PRIORITY_LOWEST);

                if (logcat !=  null) {
                    Log.d(TAG, "Stopping existing logcat monitor.");
                    logcat.stop();
                }

                Log.d(TAG, "Starting new logcat monitor.");
                logcat = new Logcat(handler);
                logcat.start();
            }
        }.start();

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle("Monitoring crashes")
                .build();

        Log.d(TAG, "Moving into foreground");
        startForeground(0, notification);

        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (logcat != null) {
            Log.d(TAG, "Stopping existing logcat monitor (in response to stopService)");
            logcat.stop();
        }

        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private static class LogcatHandler extends Handler {

        private final Context context;

        private LogcatHandler(Context context) {
            this.context = context;
        }

        @Override
        public void handleMessage(Message msg) {
            if (msg.what != Logcat.MSG_WHAT_EXCEPTION) {
                return;
            }

            if (msg.obj != null && msg.obj instanceof LoggedException) {
                Reporter.send(context, (LoggedException) msg.obj);
            }
        }
    }
}
