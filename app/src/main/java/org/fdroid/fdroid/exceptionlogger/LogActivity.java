package org.fdroid.fdroid.exceptionlogger;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import org.fdroid.fdroid.exceptionlogger.ExceptionLoggingService;

public class LogActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        startService(new Intent(this, ExceptionLoggingService.class));
    }
}
