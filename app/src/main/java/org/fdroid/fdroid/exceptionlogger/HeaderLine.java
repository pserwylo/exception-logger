package org.fdroid.fdroid.exceptionlogger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * When running `logcat -v long`, each log entry is prefixed with a header line which looks like this:
 *
 *   [ 07-12 08:50:40.644 14893:14893 E/AndroidRuntime ]
 *
 */
class HeaderLine {

    private static final Pattern PATTERN = Pattern.compile("\\[ \\d\\d-\\d\\d \\d\\d:\\d\\d:\\d\\d\\.\\d+\\s+(\\d+):\\s*(\\d+) ([VDIWEF])/(.*)\\s*]");

    public final char level;
    public final int pid;
    public final String tag;

    public static HeaderLine parse(String line) {
        // Relatively simple check to prevent having to execute the regex.
        if (line.length() < 2 || line.charAt(0) != '[' || line.charAt(line.length() - 1) != ']') {
             return null;
        }

        Matcher matcher = PATTERN.matcher(line);
        if (matcher.find()) {
            String pidGroup = matcher.group(1);
            String levelGroup = matcher.group(3);
            String tagGroup = matcher.group(4);

            return new HeaderLine(
                    levelGroup.charAt(0),
                    tagGroup.trim(),
                    Integer.parseInt(pidGroup));
        } else {
            return null;
        }
    }

    private HeaderLine(char level, String tag, int pid) {
        this.level = level;
        this.tag = tag;
        this.pid = pid;
    }

}
