package org.fdroid.fdroid.exceptionlogger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.fdroid.fdroid.exceptionlogger.ExceptionGobbler;
import org.fdroid.fdroid.exceptionlogger.LoggedException;

public class Logcat {

	public static final int MSG_WHAT_EXCEPTION = 92384720;

	private static final String TAG = "Logcat";

	private boolean mRunning = false;
	private BufferedReader mReader = null;
	private final Handler mHandler;
	private Process logcatProc;

	public Logcat(Handler handler) {
		mHandler = handler;
	}

	public void start() {
		mRunning = true;
		ExceptionGobbler gobbler = new ExceptionGobbler();

		try {
			List<String> progs = new ArrayList<>();
			progs.add("logcat");
			progs.add("-v");
			progs.add("long");
			// progs.add("-b");
			// progs.add("crash");
			progs.add("*:I");

			logcatProc = Runtime.getRuntime().exec(progs.toArray(new String[0]));
			mReader = new BufferedReader(new InputStreamReader(logcatProc.getInputStream()), 1024);

			String line;
			while (mRunning && (line = mReader.readLine()) != null) {
				if (!mRunning) {
					Log.d(TAG, "Someone requested we stop this logcat monitor, so bailing.");
					break;
				}

				LoggedException exception = gobbler.gobbleLine(line);
				if (exception != null) {
					Message message = Message.obtain(mHandler, MSG_WHAT_EXCEPTION);
					message.obj = exception;
					mHandler.sendMessage(message);
				}
			}
			Log.d(TAG, "Finished reading logcat.");
		} catch (IOException e) {
			Log.e(TAG, "error reading log", e);
		} finally {
			Log.d(TAG, "Cleaning up finished logcat session.");
			if (logcatProc != null) {
				logcatProc.destroy();
				logcatProc = null;
			}

			if (mReader != null) {
				try {
					mReader.close();
					mReader = null;
				} catch (IOException e) {
					Log.e(TAG, "error closing stream", e);
				}
			}
		}
	}

	public void stop() {
		mRunning = false;
	}
}
