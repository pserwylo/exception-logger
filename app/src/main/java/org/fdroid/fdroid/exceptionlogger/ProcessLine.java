package org.fdroid.fdroid.exceptionlogger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * When running `logcat -v long`, each log entry is prefixed with a header line which looks like this:
 *
 *   Process: org.fdroid.fdroid.crasher, PID: 14893
 *
 */
class ProcessLine {

    private static final Pattern PATTERN = Pattern.compile("Process: ([\\w._]*), PID: (\\d+)");

    public final int pid;
    public final String processName;

    public static ProcessLine parse(String line) {
        Matcher matcher = PATTERN.matcher(line);
        if (matcher.find()) {
            String processNameGroup = matcher.group(1);
            String pidGroup = matcher.group(2);

            return new ProcessLine(
                    processNameGroup,
                    Integer.parseInt(pidGroup));
        } else {
            return null;
        }
    }

    private ProcessLine(String processName, int pid) {
        this.processName = processName;
        this.pid = pid;
    }

}
