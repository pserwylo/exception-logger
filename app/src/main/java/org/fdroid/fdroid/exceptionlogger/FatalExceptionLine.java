package org.fdroid.fdroid.exceptionlogger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * When running `logcat -v long`, each log entry is prefixed with a header line which looks like this:
 *
 *   FATAL EXCEPTION: main
 *
 */
class FatalExceptionLine {

    private static final Pattern PATTERN = Pattern.compile("FATAL EXCEPTION: (.*)");

    public final String threadName;

    public static FatalExceptionLine parse(String line) {
        Matcher matcher = PATTERN.matcher(line);
        if (matcher.find()) {
            return new FatalExceptionLine(matcher.group(1));
        } else {
            return null;
        }
    }

    private FatalExceptionLine(String threadName) {
        this.threadName = threadName;
    }

}
