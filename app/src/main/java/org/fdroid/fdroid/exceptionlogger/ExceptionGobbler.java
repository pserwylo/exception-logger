package org.fdroid.fdroid.exceptionlogger;


public class ExceptionGobbler {

    private HeaderLine header = null;
    private FatalExceptionLine fatalException = null;
    private ProcessLine process = null;
    private final StringBuilder exceptionBody = new StringBuilder();

    public LoggedException gobbleLine(String logLine) {

        HeaderLine newHeader = HeaderLine.parse(logLine);

        // If we've hit a new header, for the next log entry, cleanup and return any exception we may have identified.
        if (newHeader != null) {
            LoggedException toReturn = null;
            if (fatalException != null && process != null) {
                toReturn = new LoggedException(header, fatalException, process, exceptionBody.toString());
            }

            header = newHeader;
            fatalException = null;
            process = null;
            exceptionBody.delete(0, exceptionBody.length());

            return toReturn;
        }

        if (fatalException == null) {
            fatalException = FatalExceptionLine.parse(logLine);
            return null;
        }

        if (process == null) {
            process = ProcessLine.parse(logLine);
            return null;
        }

        if (logLine.length() > 0) {
            exceptionBody.append(logLine).append('\n');
        }

        return null;
    }

}
