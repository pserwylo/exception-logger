package org.fdroid.fdroid.exceptionlogger;

@SuppressWarnings("WeakerAccess")
public class LoggedException {
    public final HeaderLine header;
    public final FatalExceptionLine fatalException;
    public final ProcessLine process;
    public final String body;

    public LoggedException(HeaderLine header, FatalExceptionLine fatalException, ProcessLine process, String body) {
        this.header = header;
        this.fatalException = fatalException;
        this.process = process;
        this.body = body;
    }

    public String getPackageName() {
        return process.processName;
    }
}
