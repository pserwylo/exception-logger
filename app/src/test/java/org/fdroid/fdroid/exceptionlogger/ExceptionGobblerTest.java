package org.fdroid.fdroid.exceptionlogger;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ExceptionGobblerTest {

    private static final String HEADER_LINE = "[ 07-12 08:50:40.644 14893:14893 E/AndroidRuntime ]";
    private static final String FATAL_EXCEPTION_LINE = "FATAL EXCEPTION: main";
    private static final String PROCESS_LINE = "Process: org.fdroid.fdroid.crasher, PID: 14893";

    private static final String[] HEADER_LINES = new String[] {
            "[ 07-12 08:48:51.173  1595: 1616 I/art      ]",
            "[ 07-12 08:50:40.642 14893:14893 D/AndroidRuntime ]",
            "[ 07-12 08:50:40.644 14893:14893 E/AndroidRuntime ]",
            "[ 07-12 08:50:40.645  1595: 2127 W/ActivityManager ]",
            "[ 07-12 08:50:40.666  2465:14931 D/DropBoxEntryAddedChimeraService ]",
            "[ 07-12 08:50:40.690  1595: 2805 I/OpenGLRenderer ]",
    };

    @Test
    public void headerParsing() {
        assertNull(HeaderLine.parse(FATAL_EXCEPTION_LINE));
        assertNull(HeaderLine.parse(PROCESS_LINE));

        HeaderLine line = HeaderLine.parse(HEADER_LINE);
        assertNotNull(line);
        assertEquals(14893, line.pid);
        assertEquals('E', line.level);
        assertEquals("AndroidRuntime", line.tag);

        for (String expected : HEADER_LINES) {
            assertNotNull("Should be header: " + expected, HeaderLine.parse(expected));
        }
    }

    @Test
    public void fatalExceptionParsing() {
        assertNull(FatalExceptionLine.parse(HEADER_LINE));
        assertNull(FatalExceptionLine.parse(PROCESS_LINE));

        FatalExceptionLine line = FatalExceptionLine.parse(FATAL_EXCEPTION_LINE);
        assertNotNull(line);
        assertEquals("main", line.threadName);

        for (String expected : HEADER_LINES) {
            assertNull("Should not be fatal exception: " + expected, FatalExceptionLine.parse(expected));
        }
    }

    @Test
    public void processParsing() {
        assertNull(ProcessLine.parse(HEADER_LINE));
        assertNull(ProcessLine.parse(FATAL_EXCEPTION_LINE));

        ProcessLine line = ProcessLine.parse(PROCESS_LINE);
        assertNotNull(line);
        assertEquals("org.fdroid.fdroid.crasher", line.processName);
        assertEquals(14893, line.pid);

        for (String expected : HEADER_LINES) {
            assertNull("Should not be process: " + expected, ProcessLine.parse(expected));
        }
    }

    @Test
    public void exceptionWithCause() {
        String[] lines = new String[]{
                "[ 07-12 09:58:33.468  3332: 3351 E/AndroidRuntime ]",
                "FATAL EXCEPTION: pool-1-thread-1",
                "Process: org.jtb.alogcat, PID: 3332",
                "java.lang.ExceptionInInitializerError",
                "	at org.fdroid.fdroid.exceptionlogger.Logcat.start(Logcat.java:74)",
                "	at org.fdroid.fdroid.exceptionlogger.LogActivity$5.run(LogActivity.java:256)",
                "	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1133)",
                "	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:607)",
                "	at java.lang.Thread.run(Thread.java:761)",
                "Caused by: java.lang.ArrayIndexOutOfBoundsException: length=3; index=3",
                "	at org.jtb.alogcat.Buffer.<clinit>(Buffer.java:21)",
                "	... 5 more",
                "",
                "[ 07-12 09:58:33.468  3332: 3351 E/AndroidRuntime ]",
        };

        ExceptionGobbler gobbler = new ExceptionGobbler();
        boolean found = false;
        for (String line : lines) {
            LoggedException e = gobbler.gobbleLine(line);
            if (e != null) {
                if (found) {
                    fail("Should only contain one exception");
                }
                found = true;

                String expectedBody =
                        "java.lang.ExceptionInInitializerError\n" +
                        "	at org.fdroid.fdroid.exceptionlogger.Logcat.start(Logcat.java:74)\n" +
                        "	at org.fdroid.fdroid.exceptionlogger.LogActivity$5.run(LogActivity.java:256)\n" +
                        "	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1133)\n" +
                        "	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:607)\n" +
                        "	at java.lang.Thread.run(Thread.java:761)\n" +
                        "Caused by: java.lang.ArrayIndexOutOfBoundsException: length=3; index=3\n" +
                        "	at org.jtb.alogcat.Buffer.<clinit>(Buffer.java:21)\n" +
                        "	... 5 more\n";

                assertEquals(expectedBody, e.body);
            }
        }
        assertTrue(found);
    }

    @Test
    public void capturesException() {
        String[] lines = {
                "[ 07-12 08:48:51.173  1595: 1616 I/art      ]",
                "Explicit concurrent mark sweep GC freed 9948(774KB) AllocSpace objects, 7(140KB) LOS objects, 22% free, 13MB/17MB, paused 199us total 13.307ms",
                "",
                "[ 07-12 08:50:40.642 14893:14893 D/AndroidRuntime ]",
                "Shutting down VM",
                "",
                "",
                "[ 07-12 08:50:40.644 14893:14893 E/AndroidRuntime ]",
                "FATAL EXCEPTION: main",
                "Process: org.fdroid.fdroid.crasher, PID: 14893",
                "java.lang.NumberFormatException: For input string: \"Crashing app\"",
                "	at java.lang.Integer.parseInt(Integer.java:521)",
                "	at java.lang.Integer.parseInt(Integer.java:556)",
                "	at org.fdroid.fdroid.crasher.MainActivity$1.onClick(MainActivity.java:19)",
                "	at android.view.View.performClick(View.java:5609)",
                "	at android.view.View$PerformClick.run(View.java:22267)",
                "	at android.os.Handler.handleCallback(Handler.java:751)",
                "	at android.os.Handler.dispatchMessage(Handler.java:95)",
                "	at android.os.Looper.loop(Looper.java:154)",
                "	at android.app.ActivityThread.main(ActivityThread.java:5969)",
                "	at java.lang.reflect.Method.invoke(Native Method)",
                "	at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:801)",
                "	at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:691)",
                "",
                "",
                "[ 07-12 08:50:40.645  1595: 2127 W/ActivityManager ]",
                "  Force finishing activity org.fdroid.fdroid.crasher/.MainActivity",
                "",
                "[ 07-12 08:50:40.666  2465:14931 D/DropBoxEntryAddedChimeraService ]",
                "User is not opted-in to Usage & Diagnostics.",
                "",
                "[ 07-12 08:50:40.690  1595: 2805 I/OpenGLRenderer ]",
                "Initialized EGL, version 1.4",
        };

        boolean found = false;
        ExceptionGobbler gobbler = new ExceptionGobbler();
        for (String line : lines) {
            LoggedException exception = gobbler.gobbleLine(line);
            if (exception != null) {
                if (found) {
                    fail("Should only have one exception present in here");
                }

                found = true;
                assertEquals(14893, exception.header.pid);
                assertEquals(14893, exception.process.pid);
                assertEquals("org.fdroid.fdroid.crasher", exception.process.processName);
                assertEquals("org.fdroid.fdroid.crasher", exception.getPackageName());

                String expectedBody =
                        "java.lang.NumberFormatException: For input string: \"Crashing app\"\n" +
                        "	at java.lang.Integer.parseInt(Integer.java:521)\n" +
                        "	at java.lang.Integer.parseInt(Integer.java:556)\n" +
                        "	at org.fdroid.fdroid.crasher.MainActivity$1.onClick(MainActivity.java:19)\n" +
                        "	at android.view.View.performClick(View.java:5609)\n" +
                        "	at android.view.View$PerformClick.run(View.java:22267)\n" +
                        "	at android.os.Handler.handleCallback(Handler.java:751)\n" +
                        "	at android.os.Handler.dispatchMessage(Handler.java:95)\n" +
                        "	at android.os.Looper.loop(Looper.java:154)\n" +
                        "	at android.app.ActivityThread.main(ActivityThread.java:5969)\n" +
                        "	at java.lang.reflect.Method.invoke(Native Method)\n" +
                        "	at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:801)\n" +
                        "	at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:691)\n";
                assertEquals(expectedBody, exception.body);
            }
        }
        assertTrue(found);
    }

    /**
     * When viewing the "crash" buffer after a segfault or similar, we get a bit of junk which is uninteresting and
     * we should ignore.
     */
    @Test
    public void uninterestingLogs() {
        String[] lines = {
                "[ 07-12 09:47:53.790  1307: 1307 F/libc     ]",
                "Fatal signal 6 (SIGABRT), code -6 in tid 1307 (mediaextractor)",
                "",
                "[ 07-12 09:47:53.843  1317: 1317 F/DEBUG    ]",
                "*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***",
                "",
                "",
                "[ 07-12 09:47:53.843  1317: 1317 F/DEBUG    ]",
                "Build fingerprint: 'Android/sdk_google_phone_x86_64/generic_x86_64:N/NPD35L/2872011:userdebug/test-keys'",
                "",
                "",
                "[ 07-12 09:47:53.843  1317: 1317 F/DEBUG    ]",
                "Revision: '0'",
                "",
                "",
                "[ 07-12 09:47:53.843  1317: 1317 F/DEBUG    ]",
                "ABI: 'x86'",
                "",
                "",
                "[ 07-12 09:47:53.843  1317: 1317 F/DEBUG    ]",
                "pid: 1307, tid: 1307, name: mediaextractor  >>> media.extractor <<<",
                "",
                "",
                "[ 07-12 09:47:53.843  1317: 1317 F/DEBUG    ]",
                "signal 6 (SIGABRT), code -6 (SI_TKILL), fault addr --------",
                "",
                "",
                "[ 07-12 09:47:53.843  1317: 1317 F/DEBUG    ]",
                "    eax 00000000  ebx 0000051b  ecx 0000051b  edx 00000006",
                "",
                "",
                "[ 07-12 09:47:53.843  1317: 1317 F/DEBUG    ]",
                "    esi f4d865d8  edi 00000002",
                "",
                "",
                "[ 07-12 09:47:53.843  1317: 1317 F/DEBUG    ]",
                "    xcs 00000023  xds 0000002b  xes 0000002b  xfs 00000003  xss 0000002b",
                "",
                "",
                "[ 07-12 09:47:53.843  1317: 1317 F/DEBUG    ]",
                "    eip ffffe430  ebp 0000051b  esp ffd0bfdc  flags 00000296",
                "",
                "",
                "[ 07-12 09:47:53.844  1317: 1317 F/DEBUG    ]",
                "",
                "backtrace:",
                "",
                "",
                "[ 07-12 09:47:53.844  1317: 1317 F/DEBUG    ]",
                "    #00 pc ffffe430  [vdso:ffffe000] (__kernel_vsyscall+16)",
                "",
                "",
                "[ 07-12 09:47:53.844  1317: 1317 F/DEBUG    ]",
                "    #01 pc 0008c4fc  /system/lib/libc.so (tgkill+28)",
                "",
                "",
                "[ 07-12 09:47:53.844  1317: 1317 F/DEBUG    ]",
                "    #02 pc 00087a46  /system/lib/libc.so (pthread_kill+70)",
                "",
                "",
                "[ 07-12 09:47:53.844  1317: 1317 F/DEBUG    ]",
                "    #03 pc 0002a4c0  /system/lib/libc.so (raise+36)",
                "",
                "",
                "[ 07-12 09:47:53.844  1317: 1317 F/DEBUG    ]",
                "    #04 pc 00021a4a  /system/lib/libc.so (abort+92)",
                "",
                "",
                "[ 07-12 09:47:53.844  1317: 1317 F/DEBUG    ]",
                "    #05 pc 000086d5  /system/lib/libminijail.so (log_sigsys_handler+85)",
                "",
                "",
                "[ 07-12 09:47:53.844  1317: 1317 F/DEBUG    ]",
                "    #06 pc 0002076f  /system/lib/libc.so",
                "",
                "",
                "[ 07-12 09:48:07.096  2800: 2805 F/libc     ]",
                "Fatal signal 6 (SIGABRT), code -6 in tid 2805 (Jit thread pool)",
                "",
                "[ 07-12 09:48:07.152  2830: 2830 F/DEBUG    ]",
                "*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***",
                "",
                "",
                "[ 07-12 09:48:07.152  2830: 2830 F/DEBUG    ]",
                "Build fingerprint: 'Android/sdk_google_phone_x86_64/generic_x86_64:N/NPD35L/2872011:userdebug/test-keys'",
                "",
                "",
                "[ 07-12 09:48:07.152  2830: 2830 F/DEBUG    ]",
                "Revision: '0'",
                "",
                "",
                "[ 07-12 09:48:07.152  2830: 2830 F/DEBUG    ]",
                "ABI: 'x86_64'",
                "",
                "",
                "[ 07-12 09:48:07.152  2830: 2830 F/DEBUG    ]",
                "pid: 2800, tid: 2805, name: Jit thread pool  >>> org.jtb.alogcat <<<",
                "",
                "",
                "[ 07-12 09:48:07.152  2830: 2830 F/DEBUG    ]",
                "signal 6 (SIGABRT), code -6 (SI_TKILL), fault addr --------",
                "",
                "",
                "[ 07-12 09:48:07.154  2830: 2830 F/DEBUG    ]",
                "Abort message: 'art/runtime/jdwp/jdwp_event.cc:681] Check failed: threadId != 0u (threadId=0, 0u=0) '",
                "",
                "",
                "[ 07-12 09:48:07.154  2830: 2830 F/DEBUG    ]",
                "    rax 0000000000000000  rbx 00007ffef2c164f8  rcx ffffffffffffffff  rdx 0000000000000006",
                "",
                "",
                "[ 07-12 09:48:07.154  2830: 2830 F/DEBUG    ]",
                "    rsi 0000000000000af5  rdi 0000000000000af0",
                "",
                "",
                "[ 07-12 09:48:07.154  2830: 2830 F/DEBUG    ]",
                "    r8  ffffffffffffffc0  r9  00007ffef7e47090  r10 0000000000000008  r11 0000000000000206",
                "",
                "",
                "[ 07-12 09:48:07.154  2830: 2830 F/DEBUG    ]",
                "    r12 0000000000000af5  r13 0000000000000006  r14 00007ffef2c14e78  r15 00007ffef2c14d89",
                "",
                "",
                "[ 07-12 09:48:07.154  2830: 2830 F/DEBUG    ]",
                "    cs  0000000000000033  ss  000000000000002b",
                "",
                "",
                "[ 07-12 09:48:07.154  2830: 2830 F/DEBUG    ]",
                "    rip 00007ffef691cb07  rbp 000000000000000b  rsp 00007ffef2c14c78  eflags 0000000000000206",
                "",
                "",
                "[ 07-12 09:48:07.157  2830: 2830 F/DEBUG    ]",
                "",
                "backtrace:",
                "",
                "",
                "[ 07-12 09:48:07.157  2830: 2830 F/DEBUG    ]",
                "    #00 pc 000000000008db07  /system/lib64/libc.so (tgkill+7)",
                "",
                "",
                "[ 07-12 09:48:07.157  2830: 2830 F/DEBUG    ]",
                "    #01 pc 000000000008a5a1  /system/lib64/libc.so (pthread_kill+65)",
                "",
                "",
                "[ 07-12 09:48:07.157  2830: 2830 F/DEBUG    ]",
                "    #02 pc 0000000000030241  /system/lib64/libc.so (raise+17)",
                "",
                "",
                "[ 07-12 09:48:07.157  2830: 2830 F/DEBUG    ]",
                "    #03 pc 000000000002877d  /system/lib64/libc.so (abort+77)",
                "",
                "",
                "[ 07-12 09:48:07.157  2830: 2830 F/DEBUG    ]",
                "    #04 pc 00000000004db5e6  /system/lib64/libart.so (_ZN3art7Runtime5AbortEv+358)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #05 pc 000000000014bb98  /system/lib64/libart.so (_ZN3art10LogMessageD1Ev+1240)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #06 pc 0000000000385703  /system/lib64/libart.so (_ZN3art4JDWP9JdwpState19SetWaitForJdwpTokenEm+1027)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #07 pc 0000000000384c6f  /system/lib64/libart.so (_ZN3art4JDWP9JdwpState24AcquireJdwpTokenForEventEm+95)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #08 pc 0000000000384597  /system/lib64/libart.so (_ZN3art4JDWP9JdwpState29SendRequestAndPossiblySuspendEPNS0_9ExpandBufENS0_17JdwpSuspendPolicyEm+199)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #09 pc 0000000000389a6f  /system/lib64/libart.so (_ZN3art4JDWP9JdwpState16PostClassPrepareEPNS_6mirror5ClassE+1583)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #10 pc 000000000018e36a  /system/lib64/libart.so (_ZN3art11ClassLinker11DefineClassEPNS_6ThreadEPKcmNS_6HandleINS_6mirror11ClassLoaderEEERKNS_7DexFileERKNS9_8ClassDefE+1034)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #11 pc 000000000018dddc  /system/lib64/libart.so (_ZN3art11ClassLinker26FindClassInPathClassLoaderERNS_33ScopedObjectAccessAlreadyRunnableEPNS_6ThreadEPKcmNS_6HandleINS_6mirror11ClassLoaderEEEPPNS8_5ClassE+1692)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #12 pc 000000000018ef35  /system/lib64/libart.so (_ZN3art11ClassLinker9FindClassEPNS_6ThreadEPKcNS_6HandleINS_6mirror11ClassLoaderEEE+1045)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #13 pc 0000000000173fae  /system/lib64/libart.so (_ZN3art11ClassLinker11ResolveTypeERKNS_7DexFileEtNS_6HandleINS_6mirror8DexCacheEEENS4_INS5_11ClassLoaderEEE+158)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #14 pc 00000000001ceb12  /system/lib64/libart-compiler.so (_ZN3art19HInstructionBuilder13ResolveMethodEtNS_10InvokeTypeE+1858)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #15 pc 00000000001cf075  /system/lib64/libart-compiler.so (_ZN3art19HInstructionBuilder11BuildInvokeERKNS_11InstructionEjjjbPjj+821)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #16 pc 00000000001ca4ea  /system/lib64/libart-compiler.so (_ZN3art19HInstructionBuilder21ProcessDexInstructionERKNS_11InstructionEj+906)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #17 pc 00000000001c99ad  /system/lib64/libart-compiler.so (_ZN3art19HInstructionBuilder5BuildEv+1757)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #18 pc 000000000019ee7d  /system/lib64/libart-compiler.so (_ZN3art13HGraphBuilder10BuildGraphEv+125)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #19 pc 0000000000203dba  /system/lib64/libart-compiler.so (_ZNK3art18OptimizingCompiler10TryCompileEPNS_14ArenaAllocatorEPNS_19CodeVectorAllocatorEPKNS_7DexFile8CodeItemEjNS_10InvokeTypeEtjP8_jobjectRKS5_NS_6HandleINS_6mirror8DexCacheEEEb+3978)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #20 pc 00000000002064bd  /system/lib64/libart-compiler.so (_ZN3art18OptimizingCompiler10JitCompileEPNS_6ThreadEPNS_3jit12JitCodeCacheEPNS_9ArtMethodEb+589)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #21 pc 000000000018c9d7  /system/lib64/libart-compiler.so (_ZN3art3jit11JitCompiler13CompileMethodEPNS_6ThreadEPNS_9ArtMethodEb+359)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #22 pc 000000000039d284  /system/lib64/libart.so (_ZN3art3jit3Jit13CompileMethodEPNS_9ArtMethodEPNS_6ThreadEb+484)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #23 pc 000000000039f1f5  /system/lib64/libart.so (_ZN3art3jit14JitCompileTask3RunEPNS_6ThreadE+645)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #24 pc 000000000051671b  /system/lib64/libart.so (_ZN3art16ThreadPoolWorker3RunEv+75)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #25 pc 0000000000516076  /system/lib64/libart.so (_ZN3art16ThreadPoolWorker8CallbackEPv+86)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #26 pc 0000000000089711  /system/lib64/libc.so (_ZL15__pthread_startPv+177)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #27 pc 00000000000299eb  /system/lib64/libc.so (__start_thread+11)",
                "",
                "",
                "[ 07-12 09:48:07.158  2830: 2830 F/DEBUG    ]",
                "    #28 pc 000000000001ca65  /system/lib64/libc.so (__bionic_clone+53)",
                "",
                "",
                "[ 07-12 09:48:38.919  2922: 2925 F/libc     ]",
                "Fatal signal 6 (SIGABRT), code -6 in tid 2925 (Jit thread pool)",
                "",
                "[ 07-12 09:48:38.974  2944: 2944 F/DEBUG    ]",
                "*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***",
                "",
                "",
                "[ 07-12 09:48:38.975  2944: 2944 F/DEBUG    ]",
                "Build fingerprint: 'Android/sdk_google_phone_x86_64/generic_x86_64:N/NPD35L/2872011:userdebug/test-keys'",
                "",
                "",
                "[ 07-12 09:48:38.975  2944: 2944 F/DEBUG    ]",
                "Revision: '0'",
                "",
                "",
                "[ 07-12 09:48:38.975  2944: 2944 F/DEBUG    ]",
                "ABI: 'x86_64'",
                "",
                "",
                "[ 07-12 09:48:38.975  2944: 2944 F/DEBUG    ]",
                "pid: 2922, tid: 2925, name: Jit thread pool  >>> org.jtb.alogcat <<<",
                "",
                "",
                "[ 07-12 09:48:38.975  2944: 2944 F/DEBUG    ]",
                "signal 6 (SIGABRT), code -6 (SI_TKILL), fault addr --------",
                "",
                "",
                "[ 07-12 09:48:38.975  2944: 2944 F/DEBUG    ]",
                "Abort message: 'art/runtime/jdwp/jdwp_event.cc:681] Check failed: threadId != 0u (threadId=0, 0u=0) '",
                "",
                "",
                "[ 07-12 09:48:38.975  2944: 2944 F/DEBUG    ]",
                "    rax 0000000000000000  rbx 00007ffef2c164f8  rcx ffffffffffffffff  rdx 0000000000000006",
                "",
                "",
                "[ 07-12 09:48:38.975  2944: 2944 F/DEBUG    ]",
                "    rsi 0000000000000b6d  rdi 0000000000000b6a",
                "",
                "",
                "[ 07-12 09:48:38.975  2944: 2944 F/DEBUG    ]",
                "    r8  ffffffffffffffc0  r9  00007ffef7e47090  r10 0000000000000008  r11 0000000000000206",
                "",
                "",
                "[ 07-12 09:48:38.975  2944: 2944 F/DEBUG    ]",
                "    r12 0000000000000b6d  r13 0000000000000006  r14 00007ffef2c14e78  r15 00007ffef2c14d89",
                "",
                "",
                "[ 07-12 09:48:38.976  2944: 2944 F/DEBUG    ]",
                "    cs  0000000000000033  ss  000000000000002b",
                "",
                "",
                "[ 07-12 09:48:38.976  2944: 2944 F/DEBUG    ]",
                "    rip 00007ffef691cb07  rbp 000000000000000b  rsp 00007ffef2c14c78  eflags 0000000000000206",
                "",
                "",
                "[ 07-12 09:48:38.980  2944: 2944 F/DEBUG    ]",
                "",
                "backtrace:",
                "",
                "",
                "[ 07-12 09:48:38.980  2944: 2944 F/DEBUG    ]",
                "    #00 pc 000000000008db07  /system/lib64/libc.so (tgkill+7)",
                "",
                "",
                "[ 07-12 09:48:38.980  2944: 2944 F/DEBUG    ]",
                "    #01 pc 000000000008a5a1  /system/lib64/libc.so (pthread_kill+65)",
                "",
                "",
                "[ 07-12 09:48:38.980  2944: 2944 F/DEBUG    ]",
                "    #02 pc 0000000000030241  /system/lib64/libc.so (raise+17)",
                "",
                "",
                "[ 07-12 09:48:38.980  2944: 2944 F/DEBUG    ]",
                "    #03 pc 000000000002877d  /system/lib64/libc.so (abort+77)",
                "",
                "",
                "[ 07-12 09:48:38.980  2944: 2944 F/DEBUG    ]",
                "    #04 pc 00000000004db5e6  /system/lib64/libart.so (_ZN3art7Runtime5AbortEv+358)",
                "",
                "",
                "[ 07-12 09:48:38.980  2944: 2944 F/DEBUG    ]",
                "    #05 pc 000000000014bb98  /system/lib64/libart.so (_ZN3art10LogMessageD1Ev+1240)",
                "",
                "",
                "[ 07-12 09:48:38.980  2944: 2944 F/DEBUG    ]",
                "    #06 pc 0000000000385703  /system/lib64/libart.so (_ZN3art4JDWP9JdwpState19SetWaitForJdwpTokenEm+1027)",
                "",
                "",
                "[ 07-12 09:48:38.980  2944: 2944 F/DEBUG    ]",
                "    #07 pc 0000000000384c6f  /system/lib64/libart.so (_ZN3art4JDWP9JdwpState24AcquireJdwpTokenForEventEm+95)",
                "",
                "",
                "[ 07-12 09:48:38.980  2944: 2944 F/DEBUG    ]",
                "    #08 pc 0000000000384597  /system/lib64/libart.so (_ZN3art4JDWP9JdwpState29SendRequestAndPossiblySuspendEPNS0_9ExpandBufENS0_17JdwpSuspendPolicyEm+199)",
                "",
                "",
                "[ 07-12 09:48:38.980  2944: 2944 F/DEBUG    ]",
                "    #09 pc 0000000000389a6f  /system/lib64/libart.so (_ZN3art4JDWP9JdwpState16PostClassPrepareEPNS_6mirror5ClassE+1583)",
                "",
                "",
                "[ 07-12 09:48:38.980  2944: 2944 F/DEBUG    ]",
                "    #10 pc 000000000018e36a  /system/lib64/libart.so (_ZN3art11ClassLinker11DefineClassEPNS_6ThreadEPKcmNS_6HandleINS_6mirror11ClassLoaderEEERKNS_7DexFileERKNS9_8ClassDefE+1034)",
                "",
                "",
                "[ 07-12 09:48:38.980  2944: 2944 F/DEBUG    ]",
                "    #11 pc 000000000018dddc  /system/lib64/libart.so (_ZN3art11ClassLinker26FindClassInPathClassLoaderERNS_33ScopedObjectAccessAlreadyRunnableEPNS_6ThreadEPKcmNS_6HandleINS_6mirror11ClassLoaderEEEPPNS8_5ClassE+1692)",
                "",
                "",
                "[ 07-12 09:48:38.981  2944: 2944 F/DEBUG    ]",
                "    #12 pc 000000000018ef35  /system/lib64/libart.so (_ZN3art11ClassLinker9FindClassEPNS_6ThreadEPKcNS_6HandleINS_6mirror11ClassLoaderEEE+1045)",
                "",
                "",
                "[ 07-12 09:48:38.981  2944: 2944 F/DEBUG    ]",
                "    #13 pc 0000000000173fae  /system/lib64/libart.so (_ZN3art11ClassLinker11ResolveTypeERKNS_7DexFileEtNS_6HandleINS_6mirror8DexCacheEEENS4_INS5_11ClassLoaderEEE+158)",
                "",
                "",
                "[ 07-12 09:48:38.981  2944: 2944 F/DEBUG    ]",
                "    #14 pc 00000000001ceb12  /system/lib64/libart-compiler.so (_ZN3art19HInstructionBuilder13ResolveMethodEtNS_10InvokeTypeE+1858)",
                "",
                "",
                "[ 07-12 09:48:38.981  2944: 2944 F/DEBUG    ]",
                "    #15 pc 00000000001cf075  /system/lib64/libart-compiler.so (_ZN3art19HInstructionBuilder11BuildInvokeERKNS_11InstructionEjjjbPjj+821)",
                "",
                "",
                "[ 07-12 09:48:38.981  2944: 2944 F/DEBUG    ]",
                "    #16 pc 00000000001ca4ea  /system/lib64/libart-compiler.so (_ZN3art19HInstructionBuilder21ProcessDexInstructionERKNS_11InstructionEj+906)",
                "",
                "",
                "[ 07-12 09:48:38.981  2944: 2944 F/DEBUG    ]",
                "    #17 pc 00000000001c99ad  /system/lib64/libart-compiler.so (_ZN3art19HInstructionBuilder5BuildEv+1757)",
                "",
                "",
                "[ 07-12 09:48:38.981  2944: 2944 F/DEBUG    ]",
                "    #18 pc 000000000019ee7d  /system/lib64/libart-compiler.so (_ZN3art13HGraphBuilder10BuildGraphEv+125)",
                "",
                "",
                "[ 07-12 09:48:38.981  2944: 2944 F/DEBUG    ]",
                "    #19 pc 0000000000203dba  /system/lib64/libart-compiler.so (_ZNK3art18OptimizingCompiler10TryCompileEPNS_14ArenaAllocatorEPNS_19CodeVectorAllocatorEPKNS_7DexFile8CodeItemEjNS_10InvokeTypeEtjP8_jobjectRKS5_NS_6HandleINS_6mirror8DexCacheEEEb+3978)",
                "",
                "",
                "[ 07-12 09:48:38.981  2944: 2944 F/DEBUG    ]",
                "    #20 pc 00000000002064bd  /system/lib64/libart-compiler.so (_ZN3art18OptimizingCompiler10JitCompileEPNS_6ThreadEPNS_3jit12JitCodeCacheEPNS_9ArtMethodEb+589)",
                "",
                "",
                "[ 07-12 09:48:38.981  2944: 2944 F/DEBUG    ]",
                "    #21 pc 000000000018c9d7  /system/lib64/libart-compiler.so (_ZN3art3jit11JitCompiler13CompileMethodEPNS_6ThreadEPNS_9ArtMethodEb+359)",
                "",
                "",
                "[ 07-12 09:48:38.981  2944: 2944 F/DEBUG    ]",
                "    #22 pc 000000000039d284  /system/lib64/libart.so (_ZN3art3jit3Jit13CompileMethodEPNS_9ArtMethodEPNS_6ThreadEb+484)",
                "",
                "",
                "[ 07-12 09:48:38.981  2944: 2944 F/DEBUG    ]",
                "    #23 pc 000000000039f1f5  /system/lib64/libart.so (_ZN3art3jit14JitCompileTask3RunEPNS_6ThreadE+645)",
                "",
                "",
                "[ 07-12 09:48:38.981  2944: 2944 F/DEBUG    ]",
                "    #24 pc 000000000051671b  /system/lib64/libart.so (_ZN3art16ThreadPoolWorker3RunEv+75)",
                "",
                "",
                "[ 07-12 09:48:38.981  2944: 2944 F/DEBUG    ]",
                "    #25 pc 0000000000516076  /system/lib64/libart.so (_ZN3art16ThreadPoolWorker8CallbackEPv+86)",
                "",
                "",
                "[ 07-12 09:48:38.981  2944: 2944 F/DEBUG    ]",
                "    #26 pc 0000000000089711  /system/lib64/libc.so (_ZL15__pthread_startPv+177)",
                "",
                "",
                "[ 07-12 09:48:38.981  2944: 2944 F/DEBUG    ]",
                "    #27 pc 00000000000299eb  /system/lib64/libc.so (__start_thread+11)",
                "",
                "",
                "[ 07-12 09:48:38.981  2944: 2944 F/DEBUG    ]",
                "    #28 pc 000000000001ca65  /system/lib64/libc.so (__bionic_clone+53)",
                "",
        };

        ExceptionGobbler gobbler = new ExceptionGobbler();
        for (String line : lines) {
            assertNull(gobbler.gobbleLine(line));
        }
    }
}
